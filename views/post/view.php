<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
           // 'category',
			[ // category name and not number
                'attribute' => 'category',
                'label' => 'Category',
                'value' => function($model){
                    return $model->categoryName->category_name;
                            },
            ],

            //'author',
			[ // authors name and not number
                'attribute' => 'author',
                'label' => 'Author',
                'format'=>'html',
				'value' => 			
							 Html::a($model->user->name, 
					['user/view', 'id' => $model->user->id]),

							
            ],
            //'status',
			[ // authors name and not number
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function($model){
                    return $model->statusItem->status_name;
                            },
            ],
			
            //'created_at',
			[ 
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],
            //'updated_at',
			[ 
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],
            //'created_by',
			[ 
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->name) ? $model->createdBy->name : 'No one!',	
			],
            //'updated_by',
			[ 
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updatedBy->name) ? $model->createdBy->name : 'No one!',	
			],
        ],
    ]) ?>

</div>
