<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

class AddruleController extends Controller
{
	public function actionOwnAuthor()
	{	
		$auth = Yii::$app->authManager;	
		$rule = new \app\rbac\OwnAuthorRule;
		$auth->add($rule);
	}
}


